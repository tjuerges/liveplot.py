#! /usr/bin/env python

'''
Plot the actual and requested values for azimuth and elevation of a MeerKAT AP.
'''


import argparse
import textwrap
import time
import copy
import threading
import collections
import numpy
import matplotlib
# On Mac OSX set this to whatever.  :-)
matplotlib.use("qt4agg")
import matplotlib.pyplot
import katcp
from katcp import Message, Sensor, CallbackClient, BlockingClient


# Should timers or threads be used?
useGuiThreads = True
useKatThreads = True
# The sensor poll interval in seconds.
updateInterval = 1.0
# The plot update interval in seconds.
plotInterval = 1.0


def pollSensor(client, sensorName):
    '''
    Poll sensor data of an AP.
    '''
    reply, informs = client.blocking_request(
        katcp.Message.request("sensor-value", sensorName))
    # If the reply is valid, valid will be True.
    valid = reply.reply_ok()
    if valid is True:
        # The value and timestamp are hidden in the parts of the reply.
        value = float(informs[0].arguments[4])
        timestamp = float(informs[0].arguments[0])
    else:
        # Signal that the returned values are invalid independently of the
        # valid flag.
        value = 0.0
        timestamp = -1.0
    return (valid, timestamp, value)

def pollPosition(client, current = True):
    '''
    Poll the "[actual|requested]-[azim|elev]" sensors of an AP.
    '''
    # Depending on the value of which either the current or the requested
    # position will be polled.
    if current is True:
        which = "actual-"
    else:
        which = "requested-"
    # Be a nice guy and initialise the used variables.
    # Not sure if Python needs that though.
    # Need to advance to the next Jedi-Python level!
    valid, timestamp, x, y = (False, -1.0, -999.9, -999.9)
    valid, timestamp, x = pollSensor(client, which + "azim")
    # Only poll the elevation position if the azimuth position is valid.
    if valid is True and timestamp != -1.0:
        valid, timestamp, y = pollSensor(client, which + "elev")
        if valid is False or timestamp == -1.0:
            # Setting the x/y coordinates to -999.9 ensures that those
            # points will be invisible in the plot.
            valid, timestamp, x, y = (False, -1.0, -999.9, -999.9)
    return (valid, timestamp, x, y)

def updateData(client, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Update circular buffers that contain sensor data.
    '''
    # Poll the new data from the AP and append it to the circular buffers.
    # Poll the actual position first.
    valid, timestamp, x, y = pollPosition(client, True)
    actualLock.acquire()
    lastEntry = len(actualValues[0]) - 1
    # Only append the new position if it is different from the last one.
    if lastEntry >= 0 and (x != actualValues[0][lastEntry] or y != actualValues[1][lastEntry]):
        actualValues[0].append(x)
        actualValues[1].append(y)
    # Or append it if it is the first entry.
    elif lastEntry == -1:
        actualValues[0].append(x)
        actualValues[1].append(y)
    actualLock.release()

    # And now poll the latest requested position.
    valid, timestamp, x, y = pollPosition(client, False)
    requestedLock.acquire()
    lastEntry = len(requestedValues[0]) - 1
    # Only append the new position if it is different from the last one.
    if lastEntry >= 0 and (x != requestedValues[0][lastEntry] or y != requestedValues[1][lastEntry]):
        requestedValues[0].append(x)
        requestedValues[1].append(y)
    # Or append it if it is the first entry.
    elif lastEntry == -1:
        requestedValues[0].append(x)
        requestedValues[1].append(y)
    requestedLock.release()

def updateDataTimer(client, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Bridge-function for matplotlib/Python timers.
    '''
    updateData(client, actualLock, requestedLock, actualValues, requestedValues)

def updateDataThread(interval, client, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Bridge-function for Python threads.
    '''
    global closed
    while closed is False:
        updateData(client, actualLock, requestedLock, actualValues, requestedValues)
        time.sleep(interval)


def copyPlotData(lock, values):
    '''
    Copy the data from the circular buffer for sensor values to two arrays for
    x and y values.  Those values will be plotted later.
    The copying is protected by a mutex to avoid race conditions.  Also the
    last item in the original circular buffer is kept.  This avoids gaps in the
    line plots because this last point is plotted in two consecutive runs and
    hence "connecting" the individual point sets.
    '''
    # Acquire the lock for that specific circular buffer.
    lock.acquire()
    localValues = [copy.deepcopy(values[0]), copy.deepcopy(values[1])]
    values[0].clear()
    values[1].clear()
    if len(localValues[0]) > 0:
        values[0].append(localValues[0][-1])
        values[1].append(localValues[1][-1])
    lock.release()
    xdata = numpy.array(localValues[0])
    ydata = numpy.array(localValues[1])
    return (xdata, ydata)

def updatePlot(figure, axes, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Plot all items in the currentValues circular buffer.
    '''
    # Copy the data from the sensor value array to an array for x and one for y.
    xdata, ydata = copyPlotData(actualLock, actualValues)
    # Plot the arrays.
    actData = axes.plot(xdata, ydata, linestyle = "-", color = "blue", marker = ".")
    del(xdata)
    del(ydata)
    # Dito.
    xdata, ydata = copyPlotData(requestedLock, requestedValues)
    reqData = axes.plot(xdata, ydata, linestyle = "", color = "red", marker = "x")
    del(xdata)
    del(ydata)
    # Display the updated plot.
    figure.canvas.draw()

def updatePlotTimer(figure, axes, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Bridge-function for matplotlib/Python timers.
    '''
    updatePlot(figure, axes, actualLock, requestedLock, actualValues, requestedValues)

def updatePlotThread(interval, figure, axes, actualLock, requestedLock, actualValues, requestedValues):
    '''
    Bridge-function for Python threads.
    '''
    global closed
    while closed is False:
        updatePlot(figure, axes, actualLock, requestedLock, actualValues, requestedValues)
        time.sleep(interval)


def addTrimmingsToPlot():
    '''
    Add the axes limits and labels, the grid and the title to the plot.
    '''
    global axes
    # Erase everything.
    axes.cla()
    # x-axis represents azimuth.
    # Would be nice if I coud just ask the sensor for its limits and use those.
    axes.set_xlim(-190.0, 280.0)
    # y-axis represents elevation.
    # Dito.
    axes.set_ylim(10.0, 97.0)
    # Display a grid.
    matplotlib.pyplot.grid(True)
    # Set the plot title.
    matplotlib.pyplot.title("MeerKAT AP position plot")
    # Label the axes.
    matplotlib.pyplot.xlabel("Azimuth (degrees)")
    matplotlib.pyplot.ylabel("Elevation (degrees)")


def closeEvent(event):
    '''
    When the close window button is clicked this function gets called.  It
    simply sets the "Goodbye!"-flag to True.
    '''
    global closed
    closed = True

def keyPressEvent(event):
    '''
    Handle key-press events.
    Clear the plotting canvas when the "backspace" or "delete" key is pressed.
    '''
    if event.key == "s" or event.key == "S":
        # Save the current plot.
        fileName = time.strftime("%FT%T-MeerKAT_AP-Plot_of_Azimuth_and_Elevation.png", time.gmtime()).replace(":", ".")
        matplotlib.pyplot.savefig(fileName, dpi = 600, orientation = "landscape", papertype = "a4", format = "png")
    elif event.key == "delete" or event.key == "backspace":
        addTrimmingsToPlot()

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("--ip", dest = "host", required = True,
    help = textwrap.dedent("IP address of AP"))
parser.add_argument("--port", dest = "port", default = 55000,
    help = "Port of AP")
args = parser.parse_args()
host, port = args.host, args.port

# Containers for the sensor values.  I am sampling the sensors every
# updateInterval seconds and plot them every plotInterval seconds.
# Hence I need a buffer of a minimum size of plotInterval / updateInterval.
# I am doubling the history buffer to be on the safe side if the Python
# internal timing gets out of whack.
historySize = 1
if plotInterval > updateInterval:
    historySize = 2 * int(round(abs(plotInterval) / abs(updateInterval)))
print "The plot will contain a maximum of %d positions." % (historySize)
actualValues = [collections.deque(maxlen = historySize), collections.deque(maxlen = historySize)]
requestedValues = [collections.deque(maxlen = historySize), collections.deque(maxlen = historySize)]

# Set-up is done.  Get cracking!
# Signals that the script should stop and quit.
closed = False

# Connect to the ACU and start the client.
print "Connecting to the ACU..."
client = katcp.BlockingClient(host, port)
client.start()

# Set-up of the matplotlib stuff.
print "Set up the matplotlib canvas..."
figure, axes = matplotlib.pyplot.subplots()

# Add limits, labels, title and grid to the plot.
addTrimmingsToPlot()

# Plot commands add to the plot and do not replace it.
axes.hold(True)

# Update the figure.
figure.canvas.draw()

# Create an event handler for the close_event.
figure.canvas.mpl_connect("close_event", closeEvent)

# Create an event handler for the key_press_event.
figure.canvas.mpl_connect("key_press_event", keyPressEvent)

# Create access locks that prevent race conditions.
# At leat I hope so.  I do not know how good Python is here.
actualLock = threading.Lock()
requestedLock = threading.Lock()

# Create a new timer object or thread  with the interval set to updateInterval
# milliseconds.  The thread or timer will call updateData.
if useKatThreads is True:
    updater = threading.Thread(target = updateDataThread, args = (updateInterval, client, actualLock, requestedLock, actualValues, requestedValues, ))
else:
    updater = figure.canvas.new_timer(interval = int(updateInterval * 1000.0))
    updater.add_callback(updateDataTimer, client, actualLock, requestedLock, actualValues, requestedValues)

# Create a new timer object or thread with the interval set to plotInterval
# milliseconds.  The thread or timer will call updatePlot.
if useGuiThreads is True:
    plotter = threading.Thread(target = updatePlotThread, args = (plotInterval, figure, axes, actualLock, requestedLock, actualValues, requestedValues, ))
else:
    plotter = figure.canvas.new_timer(interval = int(plotInterval * 1000.0))
    plotter.add_callback(updatePlotTimer, figure, axes, actualLock, requestedLock, actualValues, requestedValues)

# Start the threads/timers.
updater.start()
plotter.start()

# Start everything.  This call blocks until the window close button is clicked.
matplotlib.pyplot.show()

# First stop the update timer.
print "Stopping the data update and the plotting."
if useGuiThreads is True:
    plotter.join()
else:
    plotter.stop()

if useKatThreads is True:
    updater.join()
else:
    updater.stop()

# Wait for the timer functions to run a last time.
time.sleep(updateInterval + plotInterval)

# Now shut down the katcp client.
print "Stopping the katcp client."
client.stop()
client.join()

# That's it.  Thanks and goodbye!
matplotlib.pyplot.close(figure)
print "Goodbye!"
